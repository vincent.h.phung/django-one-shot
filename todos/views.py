from django.shortcuts import render, redirect
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from todos.models import TodoList, TodoItem
from django.urls import reverse_lazy, reverse


class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class ListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]
    # success_url = reverse_lazy("todo_list_list")

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/update.html"
    fields = ["name"]
    # success_url = "todo_detail_list"

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/create_item.html"
    fields = ["task", "due_date", "is_completed", "list"]
    # success_url = reverse_lazy("todo_list_create")

    def get_success_url(self):
        return reverse("todo_list_detail", args=[self.object.list.id])

    # def get_success_url(self):
        # # return reverse("todo_list_detail", kwargs={"pk": self.object.pk})

    # def get_form_kwargs(self, *args, **kwargs):
        # # # kwargs = super(TodoItemCreateView, self).get_form_kwargs(
            # *args, **kwargs
        # )
        # return kwargs


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/update_item.html"
    fields = ["task", "due_date", "is_completed", "list"]
    # success_url = "todo_list_detail"

    def get_success_url(self):
        return reverse("todo_list_detail", kwargs={"pk": self.object.list.pk})
